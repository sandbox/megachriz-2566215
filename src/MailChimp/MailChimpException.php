<?php

namespace Drupal\mailchimphelper\MailChimp;

use \Exception;

/**
 * Base exception class for MailChimp exceptions.
 */
class MailChimpException extends Exception {}
